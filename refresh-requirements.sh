#!/bin/sh -e
#
# Script to refresh requirement files
#
# Copyright (C) 2019-2024 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

VIRTUALENV_DIR=$(mktemp -d)
PIP_COMMAND="$VIRTUALENV_DIR/bin/pip"
DEFAULT_CONF_FILE=".$(basename "${0%.sh}.conf")"
REQUIREMENTS="
comm
grep
sed
sort
virtualenv
"

export LC_ALL=C

requirements() {
	for req in $REQUIREMENTS; do
		if ! which "$req" >/dev/null; then
			if [ -z "$has_missing" ]; then
				echo "$(basename "$0"): Missing script requirements!" 1>&2
				echo "Please install:" 1>&2
				has_missing=1
			fi
			echo " - '$req'" 1>&2
		fi
	done
	if [ -n "$has_missing" ]; then
		exit 1
	fi
}

usage() {
	cat 1>&2 <<EOF
Usage: $(basename "$0") [OPTIONS]...
Refresh requirement files

Options:
  -c FILE                   Configuration file
                              (default: '$DEFAULT_CONF_FILE')
  -e BRE                    Basic Regular Expression matching packages to be
                              completely excluded.
  -p BRE                    Basic Regular Expression matching packages to be
                              added with their compatible version.
  --help                    Print usage

Configuration:
'${conf_file:-$DEFAULT_CONF_FILE}' file is sourced and may contain the following
environment variables:

  REFRESH_REQUIREMENTS_EXCLUDE_REGEXP
                            Basic Regular Expression matching packages to be
                              completely excluded.
  REFRESH_REQUIREMENTS_COMPATIBLE_REGEXP
                            Basic Regular Expression matching packages to be
                              added with their compatible version.

EOF
	exit 1
}

parse_args() {
	unset conf_file
	unset exclude_regexp
	unset compatible_regexp

	while [ $# -gt 0 ]; do
		arg="$1"
		case $arg in
			-c)
				if [ -n "$conf_file" ] || [ $# -le 1 ]; then
					usage
				fi
				shift
				conf_file="$1"
				;;
			-e)
				if [ -n "$exclude_regexp" ] || [ $# -le 1 ]; then
					usage
				fi
				shift
				exclude_regexp="$1"
				;;
			-p)
				if [ -n "$compatible_regexp" ] || [ $# -le 1 ]; then
					usage
				fi
				shift
				compatible_regexp="$1"
				;;
			--help)
				usage
				;;
			*)
				usage
				;;
		esac
		shift
	done
}

load_conf() {
	if [ -f "${conf_file:-$DEFAULT_CONF_FILE}" ]; then
		# shellcheck disable=SC1090
		. "$(realpath "${conf_file:-$DEFAULT_CONF_FILE}")"
	fi

	exclude_regexp="${exclude_regexp:-$REFRESH_REQUIREMENTS_EXCLUDE_REGEXP}"
	compatible_regexp="${compatible_regexp:-$REFRESH_REQUIREMENTS_COMPATIBLE_REGEXP}"
}

generate_from_setuptools() {
	exclude_regexp="$1"
	compatible_regexp="$2"

	# Install package with dependencies
	"$PIP_COMMAND" install --no-cache-dir --force-reinstall .

	# Create file header warnings
	for filename in constraints.txt requirements.txt requirements-dev.txt; do
		cat << EOF > "$filename"
# This is a generated file; DO NOT EDIT!
#
# Please edit 'setup.cfg' to modify top-level dependencies and run
# '.scm-ci-helpers/refresh-requirements-docker.sh' to regenerate this file

EOF
	done

	# Create requirements file from installed dependencies
	_tmp_requirements=$(mktemp)
	"$PIP_COMMAND" freeze | grep -v "$exclude_regexp" | sort > "$_tmp_requirements"
	cat "$_tmp_requirements" >> requirements.txt

	# Install development package with dependencies
	"$PIP_COMMAND" install --no-cache-dir .[dev]

	# Create development requirements file from installed dependencies
	echo "-r requirements.txt" >> requirements-dev.txt
	_tmp_requirements_dev=$(mktemp)
	"$PIP_COMMAND" freeze | grep -v "$exclude_regexp" | sort > "$_tmp_requirements_dev"
	comm -13 - "$_tmp_requirements_dev" < "$_tmp_requirements" >> requirements-dev.txt

	# Create constraints file from installed dependencies
	cat "$_tmp_requirements_dev" >> constraints.txt

	# Cleanup
	rm -f "$_tmp_requirements"
	rm -f "$_tmp_requirements_dev"

	# Set compatible release packages
	if [ -n "$compatible_regexp" ]; then
		sed -i 's/'"$compatible_regexp"'==\([0-9]\+\)\(\.[0-9]\+\)\+$/\1~=\2.0/' requirements.txt
		sed -i 's/'"$compatible_regexp"'==\([0-9]\+\)\(\.[0-9]\+\)\+$/\1~=\2.0/' requirements-dev.txt
		sed -i 's/'"$compatible_regexp"'==\([0-9]\+\)\(\.[0-9]\+\)\+$/\1~=\2.0/' constraints.txt
	fi
}

generate_from_requirements() {
	exclude_regexp="$1"
	compatible_regexp="$2"

	# Install requirements
	if [ -f requirements-dev.txt ]; then
		"$PIP_COMMAND" install --no-cache-dir --force-reinstall -r requirements.txt -r requirements-dev.txt
	else
		"$PIP_COMMAND" install --no-cache-dir --force-reinstall -r requirements.txt
	fi

	# Create file header warning
	cat << EOF > constraints.txt
# This is a generated file; DO NOT EDIT!
#
# Please edit 'requirements.txt' or/and 'requirements-dev.txt'
# and run '.scm-ci-helpers/refresh-requirements-docker.sh' to regenerate
# this file

EOF
	# Create constraints file from installed dependencies
	"$PIP_COMMAND" freeze | grep -v "$exclude_regexp" | sort >> constraints.txt

	# Set compatible release packages
	if [ -n "$compatible_regexp" ]; then
		sed -i 's/'"$compatible_regexp"'==\([0-9]\+\)\(\.[0-9]\+\)\+$/\1~=\2.0/' constraints.txt
	fi
}

refresh_requirements() {
	exclude_regexp="$1"
	compatible_regexp="$2"

	# Create virtualenv
	virtualenv "$VIRTUALENV_DIR"

	# Generate files
	if [ -s setup.py ]; then
		generate_from_setuptools "$exclude_regexp" "$compatible_regexp"
	else
		generate_from_requirements "$exclude_regexp" "$compatible_regexp"
	fi
	
	# Verify dependency compatibility
	"$PIP_COMMAND" check

	# Cleanup
	rm -rf "$VIRTUALENV_DIR"
}

main() {
	requirements
	parse_args "$@"
	load_conf
	refresh_requirements "$exclude_regexp" "$compatible_regexp"
}

main "$@"
