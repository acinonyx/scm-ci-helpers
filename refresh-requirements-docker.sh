#!/bin/sh -e
#
# Script to refresh requirement files using a Docker container
#
# Copyright (C) 2022-2025 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

DEFAULT_REFRESH_REQUIREMENTS_DOCKER_IMAGE="python:3.11.9-alpine3.20"
DEFAULT_REFRESH_REQUIREMENTS_SCRIPT_PATH=".scm-ci-helpers/refresh-requirements.sh"

DEFAULT_CONF_FILE=".$(basename "${0%.sh}.conf")"
REQUIREMENTS="docker"

requirements() {
	for req in $REQUIREMENTS; do
		if ! which "$req" >/dev/null; then
			if [ -z "$has_missing" ]; then
				echo "$(basename "$0"): Missing script requirements!" 1>&2
				echo "Please install:" 1>&2
				has_missing=1
			fi
			echo " - '$req'" 1>&2
		fi
	done
	if [ -n "$has_missing" ]; then
		exit 1
	fi
}

usage() {
	cat 1>&2 <<EOF
Usage: $(basename "$0") [OPTIONS]...
Refresh requirement files in a Docker container

Options:
  -c FILE                   Configuration file
                              (default: '$DEFAULT_CONF_FILE')
  -i DOCKER_IMAGE           Docker image from which the container is created.
                              (default: '$DEFAULT_REFRESH_REQUIREMENTS_DOCKER_IMAGE')
  -s PATH                   Path to 'refresh-requirements.sh' relative to
                              project directory.
                              (default: '$DEFAULT_REFRESH_REQUIREMENTS_SCRIPT_PATH')
  --help                    Print usage

Configuration:
'${conf_file:-$DEFAULT_CONF_FILE}' file is sourced and may contain the following
environment variables:

  REFRESH_REQUIREMENTS_DOCKER_IMAGE
                            Docker image from which the container is created.
                              (default: '$DEFAULT_REFRESH_REQUIREMENTS_DOCKER_IMAGE')
  REFRESH_REQUIREMENTS_SCRIPT_PATH
                            Path to 'refresh-requirements.sh' relative to
                              project directory.
                              (default: '$DEFAULT_REFRESH_REQUIREMENTS_SCRIPT_PATH')

EOF
	exit 1
}

parse_args() {
	unset conf_file
	unset docker_image
	unset script_path

	while [ $# -gt 0 ]; do
		arg="$1"
		case $arg in
			-c)
				if [ -n "$conf_file" ] || [ $# -le 1 ]; then
					usage
				fi
				shift
				conf_file="$1"
				;;
			-i)
				if [ -n "$docker_image" ] || [ $# -le 1 ]; then
					usage
				fi
				shift
				docker_image="$1"
				;;
			-s)
				if [ -n "$script_path" ] || [ $# -le 1 ]; then
					usage
				fi
				shift
				script_path="$1"
				;;
			--help)
				usage
				;;
			*)
				usage
				;;
		esac
		shift
	done
}

load_conf() {
	if [ -f "${conf_file:-$DEFAULT_CONF_FILE}" ]; then
		# shellcheck disable=SC1090
		. "$(realpath "${conf_file:-$DEFAULT_CONF_FILE}")"
	fi

	docker_image="${docker_image:-$REFRESH_REQUIREMENTS_DOCKER_IMAGE}"
	script_path="${script_path:-$REFRESH_REQUIREMENTS_SCRIPT_PATH}"
}

refresh_commands() {
	cat << EOF
command -v pip >/dev/null && pip install virtualenv
EOF
	cat << EOF
command -v apt-get >/dev/null && {
apt-get -y update
command -v virtualenv >/dev/null || apt-get -qy install virtualenv
[ -f /projectdir/packages.debian ] && xargs -r -a /projectdir/packages.debian apt-get -qy install
}
EOF
	cat << EOF
command -v apk >/dev/null && {
command -v virtualenv >/dev/null || apk add -qU py3-virtualenv
[ -f /projectdir/packages.alpine ] && xargs -r -a /projectdir/packages.alpine apk add -qU
}
EOF
	cat << EOF
umask $(umask)
cd /projectdir
"$script_path"
chown "$(id -u):$(id -g)" constraints.txt requirements.txt
[ -f requirements-dev.txt ] && chown "$(id -u):$(id -g)" requirements-dev.txt
EOF
}

refresh_requirements() {
	docker_image="$1"
	script_path="$2"

	refresh_commands "$script_path" | docker run -i --rm -v "$(pwd):/projectdir" "$docker_image" /bin/sh -e -s
}

main() {
	requirements
	parse_args "$@"
	load_conf
	refresh_requirements "${docker_image:-$DEFAULT_REFRESH_REQUIREMENTS_DOCKER_IMAGE}" "${script_path:-$DEFAULT_REFRESH_REQUIREMENTS_SCRIPT_PATH}"
}

main "$@"
